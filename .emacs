;; NOTES
;;
;; If emacs gives "wrong type argument: arrayp, nil", run
;; `rm ~/.emacs.d/elpa/archives/melpa/archive-contents`
;;
;; Get the documentation of a function: F1 f <function-name>
;; 
;; Show the function called by a shortcut: F1 c <shortcut>

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "d29231b2550e0d30b7d0d7fc54a7fb2aa7f47d1b110ee625c1a56b30fea3be0f" "235dc2dd925f492667232ead701c450d5c6fce978d5676e54ef9ca6dd37f6ceb" default)))
 '(package-selected-packages
   (quote
    (go-complete auto-complete evil-ediff evil-org evil-numbers evil-surround evil-leader go-mode use-package evil-visual-mark-mode))))
;(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
; '(default ((t (:family "Dina" :foundry "unknown" :slant normal :weight normal :height 80 :width normal)))))
; '(default ((t (:family "Mono" :foundry "unknown" :slant normal :weight normal :height 190 :width normal)))))

(require 'package)

;; Add package repositories
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/lisp")
;; Load themes from the folder, where I have a custom gruvbox with darker background
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;; Evil-mode: VIM key bindings
(use-package evil
  :ensure t
  :init
  (setq evil-want-C-u-scroll t)
  :config
  (use-package evil-leader
   :ensure t
   :config
   (evil-leader/set-leader ",")
   (evil-leader/set-key
    "e" 'find-file
    "b" 'switch-to-buffer
    "k" 'kill-buffer)
   (global-evil-leader-mode))

  (evil-mode 1)
  (evil-add-hjkl-bindings package-menu-mode-map 'emacs
   (kbd "H") 'package-menu-quick-help
   (kbd "/") 'evil-search-forward
   (kbd "n") 'evil-search-next
   (kbd "N") 'evil-search-previous)

  ;; Easy navigation through windows
  (define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
  (define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
  (define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
  (define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)
  (define-key evil-motion-state-map (kbd "C-h") 'evil-window-left)
  (define-key evil-motion-state-map (kbd "C-j") 'evil-window-down)
  (define-key evil-motion-state-map (kbd "C-k") 'evil-window-up)
  (define-key evil-motion-state-map (kbd "C-l") 'evil-window-right)

  (use-package evil-surround
   :ensure t
   :config
   (global-evil-surround-mode 1))

  (use-package evil-numbers
   :ensure t
   :config
   (define-key evil-normal-state-map (kbd "C-a") 'evil-numbers/inc-at-pt)
   (define-key evil-visual-state-map (kbd "C-a") 'evil-numbers/inc-at-pt)
   (define-key evil-normal-state-map (kbd "C-z") 'evil-numbers/dec-at-pt)
   (define-key evil-visual-state-map (kbd "C-z") 'evil-numbers/dec-at-pt))

  (use-package evil-org
   :ensure t)

  (use-package evil-ediff
   :ensure t)

  (use-package nlinum
   :ensure t
   :config
   (global-nlinum-mode 1))

  (use-package key-chord
   :ensure t
   :config
   (key-chord-mode 1)
   (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
   (key-chord-define evil-visual-state-map "jk" 'evil-normal-state))

   (defun my-move-key (keymap-from keymap-to key)
    "Moves key binding from one keymap to another, deleting from the old location. "
    (define-key keymap-to key (lookup-key keymap-from key))
    (define-key keymap-from key nil))
   (my-move-key evil-motion-state-map evil-normal-state-map (kbd "RET"))
   (my-move-key evil-motion-state-map evil-normal-state-map " ")
   
   ;; Allow mouse button to click UI buttons
   (define-key evil-motion-state-map [down-mouse-1] nil)
   ;; Make horizontal movement cross lines
   (define-key evil-normal-state-map (kbd "<remap> <evil-next-line>")
    'evil-next-visual-line)
   (define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>")
    'evil-previous-visual-line)
   (define-key evil-motion-state-map (kbd "<remap> <evil-next-line>")
    'evil-next-visual-line)
   (define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>")
    'evil-previous-visual-line)

   (setq-default evil-cross-lines t)
   ;; move all elements of evil-emacs-state-modes to evil-motion-state-modes
   (setq evil-motion-state-modes (append evil-emacs-state-modes evil-motion-state-modes))
   (setq evil-emacs-state-modes nil)

    ; Bind escape to quit minibuffers
   (defun minibuffer-keyboard-quit ()
    "Abort recursive edit.
    In Delete Selection mode, if the mark is active, just deactivate it;
    then it takes a second \\[keyboard-quit] to abort the minibuffer."
    (interactive)
    (if (and delete-selection-mode transient-mark-mode mark-active)
     (setq deactivate-mark  t)
     (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
     (abort-recursive-edit)))

   (define-key evil-normal-state-map [escape] 'keyboard-quit)
   (define-key evil-visual-state-map [escape] 'keyboard-quit)
   (define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
   (define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
   (define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
   (define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
   (define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)
   (global-set-key [escape] 'evil-exit-emacs-state)
)

;; Add a line marking the 80th column only in programming modes
;; Toggle with `fci-mode`
;; This extension is too problematic (doesn't play well with line-wrap)
;(use-package fill-column-indicator
;  :ensure t
;  :config
;  (setq fci-rule-color "#500000")
;  (setq fci-handle-truncate-lines nil)
;  (add-hook 'prog-mode-hook 'fci-mode))

(use-package zoom-frm
  :ensure t
  :config
  ;(define-key ctl-x-map [(control ?-)] 'zoom-out)
  ;(define-key ctl-x-map [(control ?=)] 'zoom-in))
  (define-key ctl-x-map [(control ?+)] 'zoom-in/out)
  (define-key ctl-x-map [(control ?-)] 'zoom-in/out)
  (define-key ctl-x-map [(control ?=)] 'zoom-in/out)
  (define-key ctl-x-map [(control ?0)] 'zoom-in/out))

(use-package spacemacs-theme
  :ensure t)

(use-package doremi
  :ensure t
  :config
  (setq doremi-up-keys '(?k up))
  (setq doremi-boost-up-keys '(?K M-up))
  (setq doremi-down-keys '(?j down))
  (setq doremi-boost-down-keys '(?J M-down))
  (defun doremi-font ()
   "Successively cycle among fonts, choosing by name
   Operates on the current frame."
   (interactive)
   (doremi (lambda (newval) (set-frame-font newval t) newval)
    (frame-parameter (selected-frame) 'font)
    nil                    ; ignored
    nil                    ; ignored
    (list 
    "-*-DejaVu Sans Mono-normal-normal-normal-*-*-*-*-*-c-*-*"
    "-*-Source Code Pro-normal-normal-normal-*-*-*-*-*-c-*-*"
    "-*-Go Mono-normal-normal-normal-*-*-*-*-*-c-*-*"
    "-*-Dina-normal-normal-normal-*-*-*-*-*-c-*-*")
    'extend)) ; add current font to the ring, if it's not in the list
    (use-package doremi-cmd
    :ensure t
    :config
    ;(use-package color-theme
    ;:ensure t)
    ;; M-x `doremi-custom-themes+`
    (setq doremi-custom-themes (list 'gruvbox
                                     'cyberpunk
                                     'spacemacs-dark)))
    )

;(global-set-key [f4] (lambda () (interactive) (set-face-attribute 'default (selected-frame) :height 100)))

(use-package autofit-frame
  :ensure t
  :config
  (add-hook 'after-make-frame-functions 'fit-frame)
  (add-hook 'temp-buffer-window-show-hook
            'fit-frame-if-one-window 'append))

;(use-package column-marker
;  :ensure t
;  :config
;  (add-hook 'prog-mode-hook (lambda () (interactive) (column-marker-1 80))))

(use-package autothemer
 :ensure t)

;(use-package smart-mode-line
; :ensure t
; :config
; ;(setq sml/theme 'dark)
; (sml/setup))

(use-package spaceline
 :ensure t
 :config
 (require 'spaceline-config)
 ;(setq powerline-default-separator 'slant)
 (setq powerline-default-separator nil)
 ;(spaceline-toggle-anzu-on)
 ;(spaceline-toggle-buffer-modified-on)
 ;(spaceline-toggle-flycheck-error-on)
 ;(spaceline-toggle-flycheck-warning-on)
 ;(spaceline-toggle-flycheck-info-on)
 (spaceline-toggle-line-column-on)
 (spaceline-toggle-version-control-on)
 (spaceline-toggle-buffer-id-on)
 (spaceline-toggle-major-mode-on)
 (spaceline-toggle-evil-state-on)
 (spaceline-toggle-global-on)
 (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
 (spaceline-emacs-theme)
 )

(use-package buffer-move
 :ensure t
 :config
 (define-key evil-normal-state-map (kbd "M-k")    'buf-move-up)
 (define-key evil-normal-state-map (kbd "M-j")  'buf-move-down)
 (define-key evil-normal-state-map (kbd "M-h")  'buf-move-left)
 (define-key evil-normal-state-map (kbd "M-l") 'buf-move-right)
 (define-key evil-motion-state-map (kbd "M-k")    'buf-move-up)
 (define-key evil-motion-state-map (kbd "M-j")  'buf-move-down)
 (define-key evil-motion-state-map (kbd "M-h")  'buf-move-left)
 (define-key evil-motion-state-map (kbd "M-l") 'buf-move-right)
 ;(setq buffer-move-behavior 'move)
)


;(use-package company
; :ensure t
; :config
; (add-hook 'prog-mode-hook 'company-mode)
; (define-key evil-insert-state-map (kbd "C-n") 'company-complete)
; (use-package company-go :ensure t)
; (use-package company-jedi :ensure t)
; (use-package company-math :ensure t)
; (use-package company-racer :ensure t)
; (use-package company-shell :ensure t)
; (use-package company-ghc :ensure t)
; (use-package company-ghci :ensure t)
; (use-package company-c-headers :ensure t))

(use-package auto-complete
 :ensure t
 :config
 (ac-config-default)
 ;(setq ac-use-menu-map t)
 ;;; Ignore case if completion target string doesn't include upper characters
 ;(setq ac-ignore-case 'smart)
 (setq ac-use-menu-map t)
 (define-key ac-menu-map "\C-n" 'ac-next)
 (define-key ac-menu-map "\C-p" 'ac-previous)
 (define-key ac-menu-map "\e" 'ac-stop)
 (define-key ac-completing-map "\e" 'ac-stop)
 (define-key ac-mode-map (kbd "C-n") 'auto-complete)
 (define-key evil-insert-state-map (kbd "C-n") 'auto-complete)
 (ac-set-trigger-key (kbd "C-n"))
 ;(setq ac-show-menu-immediately-on-auto-complete nil)
 (setq ac-expand-on-auto-complete t)
 (setq ac-use-fuzzy t)
 (setq ac-auto-start 3)
 (setq ac-auto-show-menu 1)
 (setq ac-dwim t)

 (use-package go-autocomplete :ensure t)
 )

;(use-package project-explorer
; :ensure t
; :config
; (evil-leader/set-key
;  "e" 'project-explorer-toggle))

;(use-package gruvbox-theme
;  :config
;  (setq gruvbox-contrast 'medium)
;  (load-theme 'gruvbox t))

;; Programming languages

;(defun evil-set-jump-fn (fn)
;  (lambda () (interactive) (evil-set-jump) (fn)))

(use-package go-mode
  ;; go-import-add, bound to C-c C-a
  ;; 
  :ensure t
  :config
  (evil-leader/set-key-for-mode 'go-mode
    "r" 'go-remove-unused-imports
    "f" (lambda () (interactive) (evil-set-jump) (go-goto-function))
    ;"f" (evil-set-jump-fn 'go-goto-function)
    "i" (lambda () (interactive) (evil-set-jump) (go-goto-imports))
    "a" 'go-import-add
    "d" 'godef-describe
    "j" 'godef-jump)
  ;(evil--jump-hook 'go-goto-function)
  (setq gofmt-command "goimports")
  (add-hook 'go-mode-hook (lambda ()
                           (add-hook 'before-save-hook 'gofmt-before-save)))
  ;(add-hook 'go-mode-hook (lambda ()
  ;                         (eval-after-load "evil-maps"
  ;                          (local-set-key (kbd "\C-]") 'godef-jump))))
  (evil-define-key 'normal go-mode-map "\C-\]" 'godef-jump)

  (use-package golint
   :ensure t)
  (use-package go-eldoc
   :ensure t
   :config
   (set-face-attribute 'eldoc-highlight-function-argument nil
                    :underline t :foreground "#fe8019"
                    :weight 'bold)
   (add-hook 'go-mode-hook 'go-eldoc-setup))
)

(use-package markdown-mode :ensure t)
(use-package json-mode :ensure t)

;(load-theme 'cyberpunk t)

;; Highlight current line
(global-hl-line-mode 1)
;; Disable hl-line-mode in visual mode
(defvar-local was-hl-line-mode-on nil)
(defun hl-line-on-maybe ()  (if was-hl-line-mode-on (setq-local global-hl-line-mode t)))
;(defun hl-line-off-maybe () (if was-hl-line-mode-on (hl-line-mode -1)))
(defun hl-line-off () (setq-local global-hl-line-mode nil))
(defun hl-line-on () (setq-local global-hl-line-mode t))
(add-hook 'global-hl-line-mode-hook
  (lambda () (if global-hl-line-mode (setq was-hl-line-mode-on t))))

(add-hook 'evil-visual-state-entry-hook 'hl-line-off)
(add-hook 'evil-visual-state-exit-hook 'hl-line-on)

;(set-face-background 'hl-line "#252525")

;; Follow the link when opening a linked file under version control
(setq vc-follow-symlinks t)
;; At least show 7 lines under/over the cursor when reaching the edges of the window
(setq scroll-conservatively 10)
(setq scroll-margin 7)
;; Don't show startup Emacs buffer
(setq inhibit-startup-screen t)
;; Hide the toolbar
(tool-bar-mode -1)
;; Hide the menu bar
(menu-bar-mode -1)
;; Hide scroll bar
(scroll-bar-mode -1)
;; Show line numbers
;; This makes pasting operatoins (or inserting in visual-block mode) extremely slow
;; Replace it by nlinum
;(global-linum-mode 1)
;; Highlight matching paren
(show-paren-mode t)
;; TODO: Add a shortcut to toggle line numbers
;; Show line and column in mode-line
(setq column-number-mode t)

;; whitespace-mode
;; free of trailing whitespace and to use 80-column width, standard indentation
(setq-default
 whitespace-line-column 80
 whitespace-style       '(face lines-tail trailing
                          space-before-tab space-after-tab))
(add-hook 'prog-mode-hook 'whitespace-mode)

(setq-default fill-column 80)

;(global-set-key [f3] (lambda () (interactive) (linum-mode)))
;(region (:foreground ,strong :inverse-video t))
;(secondary-selection (:background ,alt-background))

;; Word-wrap long lines
(setq-default word-wrap t)

;; Set identation style for C/C++
(if (>= (display-pixel-height) 1080)
 (progn ;; HiDPI IPS
  (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono 9" ))
  (set-face-attribute 'default t :font "DejaVu Sans Mono 9")
  ;(set-face-attribute 'default nil :height 80)
  (load-theme 'gruvbox t)
  (set-face-background 'region "#504945")
 )
 (progn ;; LowDPI TN
  (add-to-list 'default-frame-alist '(font . "Dina 8" ))
  (set-face-attribute 'default t :font "Dina 8")
  ;(set-face-attribute 'default nil :height 50)
  (setq gruvbox-contrast 'hard)
  (load-theme 'gruvbox t)
  (set-face-background 'hl-line "#222222")
  (set-face-background 'mode-line "#1d2021")
  (set-face-background 'mode-line-inactive "#3c3836")
))

(setq c-default-style "bsd")

;; TODO
;(require 'auto-complete)
;(global-auto-complete-mode t)
;
;(require 'autopair)
;(autopair-global-mode) ;; enable autopair in all buffers

;; TODO company mode
;; https://github.com/company-mode/company-mode/issues/180#issuecomment-55047120
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
